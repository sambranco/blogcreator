<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = "posts";

	public function media()
	{
		return $this->hasMany('BlogCreator\Media');
	}

	public function comments()
	{
		return $this->hasMany('BlogCreator\Comment', 'post');
	}

	public function blog()
	{
		return $this->belongsTo('BlogCreator\Blog');
	}

	public function repost()
	{
		return $this->hasMany('BlogCreator\Repost');
	}
	public function categories() 
	{
		return $this->belongsTo('BlogCreator\Categories');
	}
}
