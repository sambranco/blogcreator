<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Repost extends Model
{
    protected $table = 'repost';

    protected function post()
    {
    	return $this->belongsTo('BlogCreator\Post');
    }
}
