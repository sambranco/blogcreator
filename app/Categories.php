<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $table = 'categories';

	public function posts () 
	{
		return $this->hasMany('BlogCreator\Post');

	}
}