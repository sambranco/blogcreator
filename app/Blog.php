<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function user()
    {
        return $this->belongsTo('BlogCreator\User', 'owner');
    }

    public function posts()
    {
        return $this->hasMany('BlogCreator\Post');
    }
}
