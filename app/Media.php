<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    public function post()
    {
    	return $this->belongsTo('BlogCreator\Post');
    }
}
