<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/test', 'SubController@getFollowedBlogs');
*/

Route::get('/subs', 'SubController@displaySubs');

Route::get('/inbox', 'BlogController@displayInbox');
Route::post('/new', 'BlogController@createPost');
Route::get('/', 'SubController@displaySubBlogs');
Route::match(array('GET', 'POST'), '/register', 'UserController@register');
Route::post('/login', 'UserController@loginAction');
Route::get('/logout', 'UserController@logoutAction');
Route::get('/activate', 'UserController@activateAccount');
Route::match(array('GET', 'POST'), '/blog/create', 'BlogController@createBlogAction');
Route::get('/root', 'RootController@index');
Route::get('/{blog}', 'BlogController@displayBlog');

Route::get('/new/post', 'PostsController@newAdvancedPost');

Route::post('/newmsg', 'BlogController@sendMsgFromBlog');

Route::post('/comment', 'CommentsController@postComment');


Route::get('/comment/{id}', 'CommentsController@getPostComments');

Route::group(['prefix' => 'root'], function () {
	Route::get('/users', 'RootController@allUsers');
	Route::get('/blogs', 'RootController@allBlogs');
});

Route::match(array('GET', 'POST'), '/edit/profile', 'UserController@editProfile');
Route::group(['prefix' => 'new'], function () {
	Route::match(array('GET', 'POST'), '/cat', 'BlogController@createCat');
});

Route::match(array('GET', 'POST'), '/edit/{blog}', 'BlogController@editBlog');


Route::group(['prefix' => 'ajax'], function () {
	
	Route::get('/delete/{d}/{id}', 'BlogController@deletePlz');
	Route::get('/bann', 'BlogController@showBann');
	Route::get('/repost/{postId}/{to}', 'PostsController@repostPost');
});

Route::group(['prefix' => 'edit/{blog}'], function () {
	Route::get('/posts', 'PostsController@allPosts');
	Route::get('/{postId}', 'PostsController@displayPost');
});

Route::get('/follow/{f}', 'SubController@followSomeone');
Route::get('/{blog}/{id}', 'BlogController@displayPost');