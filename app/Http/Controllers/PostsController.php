<?php

namespace BlogCreator\Http\Controllers;

use Illuminate\Http\Request;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use BlogCreator\Blog;
use BlogCreator\Post;
use BlogCreator\Comment;
use View;
use Auth;
use Redirect;
use BlogCreator\Repost;

class PostsController extends Controller
{
    public function allPosts($blog)
    {
        $thisBlog = Blog::where('url', $blog)->first();
        return View::make('edit.posts')->withPosts($thisBlog);
    }

    public function displayPost($blog, $postId)
    {
        $thisPost = Post::where('id', $postId)->first();

        $postComments = $thisPost->comments;
        
        return View::make('edit.solopost')->withPost($thisPost)->withComments($postComments);
    }

    public function newAdvancedPost()
    {
        return View::make('blog.new');
    }

    /**
     * repost un article sur un autre blog
     * 
     * @param  [type] $postId id du post que l'on reposte
     * @param  [type] $to     blog sur lequel on reposte
     * @return [type]         [description]
     */
    public function repostPost($postId, $to)
    {
        $post = Post::where('id', $postId)->first();

        $blog = Blog::where('id', $to)->first();

        if ($blog->owner == Auth::user()->id) {
            $repost = new Repost;
            $repost->where = $to;
            $repost->post = $postId;
            $repost->save();

            return Redirect::back();
        }
    }
}
