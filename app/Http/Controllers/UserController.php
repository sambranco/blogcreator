<?php

namespace BlogCreator\Http\Controllers;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use View;
use Request;
use Redirect;
use BlogCreator\User;
use BlogCreator\Subscription;
use Input;
use Validator;
use Hash;
use Auth;
use Mail;
use Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function register()
    {
        if (Request::isMethod('POST')) {
            return $this->registerAction();
        } else {
            return View::make('register');
        }

    }

    public function editProfile()
    {
        $user = User::where('id', Auth::user()->id)->first();

        if (Request::isMethod('POST')) {
            $data = Input::all();

            $user->email = $data['email'];
            $user->username = $data['username'];
            $user->save();

            return Redirect::to('');
            
        } else {
            return View::make('admin.editprofile')->withUser($user);
        }

    }

    public function registerAction()
    {
        $rules = array(
            'username'=>'required|alpha|min:5|unique:users',
            'email'=>'required|unique:users|email',
            'password'=>'required|min:6'
            );

        $data = Input::all();
        $avatar = Input::file('avatar');

        $validator = Validator::make($data, $rules);

        if (!$validator->fails()) {
            $user = new User;
            $user->username = $data['username'];
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->active = 0;
            /*$user->active = 1;*/
            $user->activation_token = sha1(time());
            $user->save();

            $this->sendActivationMail($user);

            $logData = array(
                'username' => $data['username'],
                'password' => $data['password']
                );

            Auth::attempt($logData);
            return Redirect::to('/');

        } else {
            return Redirect::to('/register')->withErrors($validator)->withInput();
        }
    }

    public function sendActivationMail($user)
    {
        $mail = "Bonjour ".$user->username.",\n\nCliquez sur le lien suivant pour activer votre compte : http://blog-creator.prod/activate?id=".$user->id."&token=".$user->activation_token;

        Mail::raw($mail, function ($msg) use ($user) {
            $msg->from('tony@gmail.com', 'Tony Montana');
            $msg->to($user->email, $user->username)->subject('[blogCreator] Confirmez votre adresse e-mail');
        });
    }

    public function activateAccount()
    {
        $id = Input::get('id');
        $token = Input::get('token');

        $user = User::where('id', '=', $id)->first();

        $userToken = $user->activation_token;

        if ($token == $userToken) {
            if ($user->active == 0) {
                $user->active = 1;
                $user->save();
                $this->createUserFolder($user->username);

                return Redirect::to('/blog/create');
            }

        } else {
            return Redirect::to('/');
        }
    }

    public function createUserFolder($username)
    {

     Storage::makeDirectory($username);

 }

 public function loginAction()
 {
    $data = array(
        'username' => Input::get('username'),
        'password' => Input::get('password')
        );

    $user = User::where('username', '=', Input::get('username'))->first();

    if (Auth::attempt($data)) {
        return Redirect::to('/');
    } else {
        Auth::logout();
        return Redirect::to('/');
    }
}

public function logoutAction()
{
    if (Auth::check()) {
        Auth::logout();
        return Redirect::to('/');
    } else {
        return Redirect::to('/');
    }
}
}