<?php

namespace BlogCreator\Http\Controllers;

use Illuminate\Http\Request;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use BlogCreator\Subscription;
use BlogCreator\Blog;
use BlogCreator\Post;
use Redirect;
use BlogCreator\User;
use Auth;
use View;
use Response;

class SubController extends Controller
{
    public function followSomeone($f)
    {  
        $user = User::where('id', Auth::user()->id)->first();

        $subs = Subscription::where('follower', Auth::user()->id)->get();

        foreach ($subs as $sub) {
            if ($sub->blog == $f) {
                return Redirect::back();
            }
        }

        $go = new Subscription;
        $go->follower = Auth::user()->id;
        $go->blog = $f;
        $go->save();

        return Redirect::back();
    }

    public function getFollowedBlogs()
    {
        $userId = Auth::user()->id;
        $followedBlogs = [];

        $subs = Subscription::where('follower', $userId)->get();
        foreach ($subs as $sub) {
            $followedBlogs[] = $sub->blog;
        }

        return $followedBlogs;

    }

    public function displaySubs()
    {
        $subs = Subscription::where('follower', Auth::user()->id)->get();
        
        return View::make('admin.subs')->withSubs($subs);
    }

    public function displaySubBlogs()
    {

        if (Auth::check()) {
            $blogs = $this->getFollowedBlogs();
            return View::make('homepage.homepage')->withPosts($blogs);
        } else {
            return View::make('homepage.homepage');
        }
    }
}
