<?php

namespace BlogCreator\Http\Controllers;

use Request;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use View;
use BlogCreator\User;
use BlogCreator\Blog;
use Response;

class RootController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(1);

        if (Request::ajax()) {
            return Response::json(View::make('root.index')->with(compact('users'))->render());
        }

        return View::make('root.index')->with(compact('users'));
    }

    public function allUsers()
    {
        $users = User::paginate(1);

        if (Request::ajax()) {
            return Response::json(View::make('root.index')->with(compact('users'))->render());
        }

        return View::make('root.users')->with(compact('users'));
    }

    public function allBlogs()
    {
        $blogs = Blog::paginate(5);

        if (Request::ajax()) {
            return Response::json(View::make('root.index')->with(compact('blogs'))->render());
        }

        return View::make('root.blogs')->with(compact('blogs'));
    }

}