<?php

namespace BlogCreator\Http\Controllers;

use Illuminate\Http\Request;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use BlogCreator\Comment;
use Input;
use Redirect;
use URL;
use Auth;
use Response;
use BlogCreator\Blog;

class CommentsController extends Controller
{
    /**
     * Poste un commentaire
     * @return View
     */
    public function postComment()
    {
        $data = Input::all();
        $comment = new Comment;

        if (Auth::check()) {
            $comment->author = Auth::user()->username;
        } else {
            $comment->author = $data['author'];
        }

        $comment->comment = $data['comment'];
        $comment->post = $data['post'];
        $comment->active = 0;
        $comment->save();

        return Redirect::back();

    }

    public function getPostComments($id)
    {
        $comments = Comment::where('post', $id)->orderBy('created_at', 'desc')->get();
        
        return Response::json($comments);

    }

    public function manageComments($blog=null)
    {
        
    }
}
