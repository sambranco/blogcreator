<?php

namespace BlogCreator\Http\Controllers;

use Request;

use BlogCreator\Http\Requests;
use BlogCreator\Http\Controllers\Controller;
use URL;
use View;
use BlogCreator\Blog;
use BlogCreator\User;
use Input;
use Redirect;
use Auth;
use BlogCreator\Post;
use Storage;
use BlogCreator\Media;
use BlogCreator\Subscription;
use Validator;
use BlogCreator\Comment;
use BlogCreator\Tags;
use BlogCreator\Inbox;
use BlogCreator\Categories;

class BlogController extends Controller
{
    /**
     * type de post 
     * 0 texte 1 photo 2 citation 3 audio 4 vidéo 5 bannière
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @todo vérifier le referer
     */
    public function createBlogAction()
    {
        if (Request::isMethod('POST')) {
            $data = Input::all();

            $rules = array(
                'name' => 'required|min:4|max:50',
                'url'=>'required|min:4|max:16|unique:blog',
                'description'=>'required|min:5|max:100'
                );

            $validator = Validator::make($data, $rules);

            if (!$validator->fails()) {
                $blog = new Blog;
                $blog->name = Input::get('name');
                $blog->description = Input::get('description');
                $blog->owner = Auth::user()->id;
                $blog->url = Input::get('url');
                $blog->active = true;
                $blog->save();

                return Redirect::to('/');
            } else {
                return Redirect::to('/blog/create')->withErrors($validator)->withInput();
            }
        } else {
            return View::make('blog.create');
        }

    }

    public function createPost()
    {
        $data = Input::all();
        $type = $data['type'];

        $post = new Post;
        $post->blog_id = $data['blog'];
        $post->type = $type;
        $post->active = true;

        if ($data['cat'] != '') {
            $post->categorie = $data['cat'];
        }
        
        $post->save();
        $this->postType($type, $post, $data);
        /*   $this->addCat($post, $data);*/
        $post->save();

        return Redirect::to('/');

    }

    public function addTags($post, $data)
    {
        $tags = new Tags;
        $tags->post = $post->id;
        $tags->tags = $data['tags'];
        $tags->save();
    }

    public function createCat()
    {
        if (Request::isMethod('POST')) {
            $catName = Input::get('categorie');

            $cate = new Categories;
            $cate->name = $catName;
            $cate->save();

            return Redirect::back();
        }

        return View::make('homepage.addnewcat');
    }

    public function postType($type, $post, $data)
    {
        switch ($type) {
            case 0:
            $post->content = $data['text'];
            $post->title = $data['title'];
            $this->addTags($post, $data);
            break;
            case 1:
            $this->postFile($post, $type);
            break;
            case 2:
            $post->content = $data['text'];
            $post->author = $data['author'];
            break;
            case 3:
            $this->postFile($post, $type);
            break;
            case 4:
            $this->postFile($post, $type);
            break;
            default:
                # code...
            break;
        }
    }

    /**
    * @todo faire une fonction qui vérifie que le fichier uploadé est bien du type attendu
    */
    public function postFile($post=null, $type=null)
    {
        $dir = public_path(). '\data\\';
        $path = $dir."\\".Auth::user()->username;

        $file = Request::file('file');
        $ext = '.'.$file->getClientOriginalExtension();
        $fileName = sha1(microtime()). $ext;
        $file->move($path, $fileName);

        if ($type != 5) {
            $this->createMedia($fileName, $post, $type);
        }

        return $fileName;

    }

    public function createMedia($fileName, $post, $type)
    {        
        $media = new Media;
        $media->post_id = $post->id;
        $media->type = $type;
        $media->user = Auth::user()->id;
        $media->path = $fileName;

        $media->save();
    }

    public function displayBlog($url)
    {
        $thisBlog = Blog::where('url', $url)->first();

        if ($thisBlog) {
            return View::make('blog.display')->withBlog($thisBlog);
        }
    }

    public function displayPost($blog, $id)
    {
        $post = Post::where('id', $id)->first();
        $blog = Blog::where('url', $blog)->first();

        return View::make('blog.post.solo')->withBlog($blog)->withPost($post);

    }

    public function editBlog($blog)
    {
        $toEdit = Blog::where('url', $blog)->first();

        if ($toEdit && $toEdit->owner == Auth::user()->id) {
            if (Request::isMethod('POST')) {
                $data = Input::all();
                $toEdit->name = $data['name'];
                $toEdit->description = $data['description'];
                $toEdit->save();
                if ($data['file'])
                {
                   $this->addBann($toEdit);
               }

               return Redirect::to('/');
           }
           return View::make('blog.edit')->withBlog($toEdit);
       } else {
        return Redirect::to('/');
    }
}

public function addBann($toEdit)
{
    $bann = $this->postFile($post=null, $type=5);
    $toEdit->bann = $bann;
    $toEdit->save();
}

public function showBann($blog=null)
{
    $blog = 'clemencelux';
    var_dump(Input::get('blog'));
    die();
    $toShow = Blog::where('url', $blog)->first();

    return $toShow->bann;
}

    /**
     * 0 Blog 1 Post 2 Comment 3 Subs 4 Mail 5 user
     * 
     * supprimer tout les trucs que tu veux
     * 
     * @param  int $d type qui arrive
     * @return [type]    [description]
     */
    public function deletePlz($d,$id)
    {

        switch ($d) {
            case 0:
            Blog::where('id', $id)->first()->delete();
            break;
            case 1:
            Post::where('id', $id)->first()->delete();
            break;
            case 2:
            Comment::where('id', $id)->first()->delete();
            break;
            case 3:
            Subscription::where('id', $id)->first()->delete();
            break;
            case 4:
            Inbox::where('id', $id)->first()->delete();
            break;
            case 5:
            $user = User::where('id', $id)->first();
            $user->active = 0;
            $user->save();
            Auth::logout();

            return Redirect::to('/');
            break;
            default;
            break;

            return Redirect::back();
        }
    }

    public function sendMsgFromBlog()
    {
        $data = Input::all();

        $msg = new Inbox;

        $msg->sender = $data['author'];
        $msg->receiver = $data['receiver'];
        $msg->message = $data['message'];
        $msg->email = $data['email'];
        $msg->save();

        return Redirect::back();
    }

    public function displayInbox()
    {
        $messages = Inbox::where('receiver', Auth::user()->id)->get();

        return View::make('admin.inbox')->withMessages($messages);
    }
}