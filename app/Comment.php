<?php

namespace BlogCreator;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    public function post()
    {
    	return $this->belongsTo('BlogCreator\Post');
    }
}
