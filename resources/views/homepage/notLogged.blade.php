@extends('layout')

@section('body')
<header>
	<section class="headerRight">
		<button class="btn btn-info headerBtn" data-toggle="modal" data-target="#loginModal">Connexion</button>
		<button id="registerHeader" class="btn btn-info headerBtn">Inscription</button>
	</section>
</header>
<section class="myCont">
	<section class="bodyHome">
		<section class="container welcomeHomepage">
			<div class="jumbotron">
				<h1>blogCreator</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
			</div>
		</section>
	</section>
</section>
@include('homepage/loginModal')
@stop

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass('bgImg');
		var registerBtn = $("#registerHeader"),
		myBody = $(".bodyHome"),
		myCont = $(".myCont"),
		header = $('header');
		function appendRegister() {
			myBody.fadeOut('fast', function () {
				myBody.remove();
				myCont.load('/register', function () {
					myCont.hide().fadeIn('fast');
				});
			});
			$(this).fadeOut('slow').remove();

			var backHome = '<span class="backHome"><< Retour à l\'accueil</span>';
			header.prepend(backHome);
		}

		registerBtn.click(appendRegister);
	});
</script>
@stop