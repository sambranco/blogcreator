{!! Form::open(array('url' => '/new', 'class' => 'newPostForm', 'files' => true)) !!}
<div class="form-group">
	<select name="blog" class="form-control">
		@foreach (Auth::user()->blog()->get() as $blog)
		<option value="{{$blog->id}}">{{ $blog->name }}</option>
		@endforeach
	</select>
</div>
<div class="formWrapper">
</div>
<div class="form-group">
	{!! Form::label('categorie') !!}
	<select name="cat">
		@foreach (BlogCreator\Categories::all() as $cat)
		<option value="{{$cat->id}}">{{ $cat->name }}</option>
		@endforeach
	</select>
</div>

<div id="btnNewPost">
	<button type="button" class="btn btn-default" id="closeNewPost">Annuler</button>
	<input type="submit" class="btn btn-primary" id="okNewPost" value="Poster">
</div>
{!! Form::close() !!}