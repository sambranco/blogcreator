<div class="modal fade" id="loginModal">
    <div class="modal-dialog loginModal">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Connexion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-dismissible alert-danger errorDiv">
                </div>
                <div class="loginForm">
                    {!! Form::open(array('url' => '/login')) !!}
                    <div class="form-group">
                        {!! Form::text('username',null,array('class' => 'form-control', 'placeholder' => 'Username')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password',array('class' => 'form-control', 'placeholder' => 'Mot de passe')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <input type="submit" class="btn btn-primary" id="addNewQuestion" value="Connexion">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>