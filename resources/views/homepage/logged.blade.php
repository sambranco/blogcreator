<header>
	<h1>BlogCreator</h1>
	<div class="headerSearch">
		<input type="text" class="searchInput">
	</div>
	<div class="headerRight">
		<a href="inbox"><i class="fa fa-paper-plane headerIcon" data-toggle="tooltip" data-placement="bottom" title="Inbox"></i></a>
		<a href="subs"><i class="fa fa-users headerIcon" data-toggle="tooltip" data-placement="bottom" title="Abonnement"></i></a>
		<i class="fa fa-user headerIcon dropdown-toggle" id="userBtn" data-toggle="tooltip" data-placement="bottom" title="Moi"></i>
		<a href="new/post"><i class="fa fa-pencil-square headerIcon pencilHeader"></i></a>
	</div>
	<ul class="userMenu none" id="userMenuTop">
		@foreach (Auth::user()->blog()->get() as $blog)
		<li><a href="/{{$blog->url}}">{{ $blog->name }}</a> <a href="edit/{{$blog->url}}"><i class="fa fa-cogs"></i></a></li>
		@endforeach
		<li><a href="blog/create">Créer un blog</a></li>
		<hr>
		<li><a href="new/cat">Ajouter une catégorie</a></li>
		<li><a href="edit/profile">Mon profil</a></li>
		<li><a href="/logout" class="center">Déconnexion</a></li>
	</ul>
</header>
<section class="homePosts container">
	<div class="newPostHome">
		<div id="homePost" class="none">
			@include('homepage.newpost')
		</div>
		<div id="homePostList">
			<ul>
				<li data-title="Texte" class="newPos>	tFade"><i class="fa fa-font"></i>texte</li>
				<li data-title="Photo" class="newPostFade"><i class="fa fa-picture-o"></i></i>photo</li>
				<li data-title="Citation" class="newPostFade"><i class="fa fa-quote-right"></i>citation</li>
				<li data-title="Audio" class="newPostFade"><i class="fa fa-music"></i>audio</li>
				<li data-title="Vidéo" class="newPostFade"><i class="fa fa-video-camera"></i>vidéo</li>
			</ul>
		</div>
	</div>
	@foreach ($posts as $post)
	@foreach (BlogCreator\Post::where('blog_id', $post)->orderBy('created_at', 'desc')->get() as $soloDolo)
	<div class="soloPostHome">
		<span class="whoPostThat">{{$soloDolo->blog->user->username }}</span>
		<!-- <img src="https://36.media.tumblr.com/39a65f85bdde8f16e40d5dd91a5686ce/tumblr_nx95yvJtX01u545pyo1_540.jpg"> -->
		<!-- <img src="img/or.jpg"> -->
		<span class="contentSubs">{!! $soloDolo->content !!}</span>
		@if (!empty($soloDolo->media->first()))
		@foreach($soloDolo->media as $media)
		@if ($media->type == 1)
		<img src="{{URL::to('data/'.$soloDolo->blog->user->username.'/'.$media->path.'')}}">
		@elseif ($media->type == 3)
		<audio controls crossorigin>
			<source src="{{URL::to('data/'.$soloDolo->blog->user->username.'/'.$media->path.'')}}">
				Erreur
			</audio>
			@elseif ($media->type == 4)
			<video controls crossorigin>
				<source src="{{URL::to('data/'.$soloDolo->blog->user->username.'/'.$media->path.'')}}">
					Erreur
				</video>
				@endif
				@endforeach
				@endif
				<div class="aboutThatPost">
					{{ $soloDolo->created_at }}
				</div>
			</div>
			@endforeach
			@endforeach

		</section>
		@include('blog/post/text')
		@section('js')
		<script>
			$(document).ready(function() {
				$('[data-toggle="tooltip"]').tooltip();
				var userBtn = $('#userBtn'),
				userMenu = $(".userMenu"),
				userBtnPos = userBtn.offset().left,
				postBtn = $('.newPostFade'),
				postDiv = $('.newPostHome'),
				closeBtn = $('#closeNewPost'),
				addPhotoLink = $('.addPhotoLink'),
				inpFile = $('input[type="file"]'),
				addNewCatBtn = $('#addNewCatPost');


				var textPost = $("#textPost");

				function appearPost (e) {
					var title = $(this).data('title');
					var content = modalContent(title);
					$('#homePostList').fadeOut('fast', function () {
						$("#homePost .formWrapper").html(content);
						$('#homePost').fadeIn('fast');
					});
					postDiv.animate({height: 520}, 200);
				}
				function closePost (e) {
					$('#homePost').fadeOut('fast', function () {
						$('#homePostList').fadeIn('fast');
					});
					postDiv.animate({height: 116}, 200);
				}
				function addLinkPhoto (e) {
					console.log('ok');
				}
				function toggleUserDropdown() {
					userMenu.toggleClass('none');
					userMenu.css({left: userBtnPos - 20});
				}

				function appendNewCat() {
					$(this).fadeOut('fast', function () {
						$(this).remove();
						$('#newCat').show();

					});

				}

				function modalContent(title) {
					var content;
					switch(title) {
						case 'Texte':
						content = '<div class="form-group"><input type="text" placeholder="Titre" name="title" class="form-control"></div><div class="form-group"><textarea name="text" class="form-control" placeholder="Votre texte ici..."></textarea></div><div class="form-group"><input type="text" placeholder="Tags" name="tags" class="form-control"></div><input type="hidden" name="type" value="0">';
						break;
						case 'Photo':
						content = '<div id="wrapPhotoUpload"><div class="form-group"><input type="file" name="file"><i class="fa fa-picture-o"></i><b>Ajouter une photo</b></div><input type="hidden" name="type" value="1"></div><span class="addPhotoLink">ou depuis une URL</span></div>';
						break;
						case 'Citation':
						content = '<div class="form-group"><textarea class="form-control" name="text" placeholder="Citation"></textarea></div><div class="form-group"><input type="text" class="form-control" name="author" placeholder="Auteur"></div><input type="hidden" name="type" value="2">';
						break;
						case 'Audio':
						content = '<div class="form-group"><input type="file" name="file" class="inpFileHome"></div><input type="hidden" name="type" value="3">';
						break;
						case 'Vidéo':
						content = '<div class="form-group"><input type="file" name="file" class="inpFileHome"></div><input type="hidden" name="type" value="4">';
						break;
					}
					return content;
				}
				function newPost(e) {
					var modalTitle = $(this).find('.modal-title'),
					modalBody = $(this).find('.formWrapper'),
					title = $(e.relatedTarget).data('title');
					var content = modalContent(title);
					modalTitle.html(title);
					modalBody.html(content);
				}
				function getBlogs() {
					$.get('/test', {blog}).done(function (data) {
						console.log(data);
					});
				}
				$(".newPostModal").on('show.bs.modal', newPost);
				$(document).on('click', function (e) {
					var target = e.target;
			/*if ($(target).context.id == 'userMenuTop' || $(target).parents('.userMenu').length > 0 || $(target).context.id == 'userBtn') {
				console.log('ok');
			} else {
				$('.userMenu').toggleClass('none');
			}*/
		});
				userBtn.click(toggleUserDropdown);
				textPost.click(newPost);
				postBtn.click(appearPost);
				closeBtn.click(closePost);
				addNewCatBtn.click(appendNewCat);
				addPhotoLink.click(addLinkPhoto);
				getBlogs();
			})
</script>
@stop