@extends('layout')

@section('body')
@if (Auth::check() && Auth::user()->active == 1)
@include('homepage/logged')
@else
@include('homepage/notLogged')
@endif
@stop