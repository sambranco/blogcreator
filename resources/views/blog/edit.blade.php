@extends('layout')
@section('body')
	<h1 class="editH">{{ $blog->url }}</h1>
<section class="editBlogCont">
	<div class="well">
		{!! Form::open(array('files' => true)) !!}
		<div class="form-group">
			{!! Form::label('Nom du blog', null,array('class' => 'control-label')) !!}
			{!! Form::text('name',$blog->name, array('class' => 'form-control'))!!}
		</div>
		<div class="form-group">
			{!! Form::label('Description', null,array('class' => 'control-label')) !!}
			{!! Form::textarea('description',$blog->description, array('class' => 'form-control'))!!}
		</div>
		<div class="form-group">
			{!! Form::label('Bannière') !!}
			{!! Form::file('file')!!}
		</div>
		<div class="form-group">
			{!! Form::submit() !!}
		</div>
		{!! Form::close() !!}
	</div>
	<div class="well">
	<iframe src="<?php echo $blog->url; ?>/posts" height="500"></iframe>
	</div>
	<a href="/ajax/delete/0/{{$blog->id}}" class="btn btn-lg btn-danger">Supprimer ce blog</a>
</section>
@stop