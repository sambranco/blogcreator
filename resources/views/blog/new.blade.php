@extends('layout')

@section('body')
<section class="container">
	<div class="well">
		{!! Form::open(array('url' => '/new')) !!}
		<select name="blog">
			@foreach (Auth::user()->blog()->get() as $blog)
			<option value="{{$blog->id}}">{{ $blog->name }}</option>
			@endforeach
		</select>
		<div class="form-group">
			{!! Form::label('title',null,array('class' => 'control-label')) !!}
			{!! Form::text('title',null, array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('text',null,array('class' => 'control-label')) !!}
			{!! Form::textarea('text',null,array('class' => 'form-control', 'id' => 'txtZone')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('tags',null,array('class' => 'control-label')) !!}
			{!! Form::text('tags',null, array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::submit() !!}
		</div>
		{!! Form::hidden('type', 0) !!}
		{!! Form::close() !!}
	</div>
</section>
@stop

@section('js')
<script src="../txbx/textboxio.js"></script>

<script type="text/javascript">
	
	$(document).ready(function() {
		var editor = textboxio.replace('#txtZone');
	});
</script>
@stop