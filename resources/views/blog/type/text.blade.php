@if ($post->title)
<h2>{{ $post->title }}</h2>
@endif 

@if ($post->content)
<p>{!! $post->content !!}</p>
@endif