@extends('layout')

@section('body')
<section class="blogCreateForm">
	@if (!empty($errors))
	{{ $errors->first() }}
	@endif
	{!! Form::open() !!}
	<div class="form-group">
		{!! Form::label('name', 'Nom de votre blog') !!}
		{!! Form::text('name',null,array('class' => 'form-control blogNameInput')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('url', 'Url de votre blog') !!}
		{!! Form::text('url',null,array('class' => 'form-control blogNameInput')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('description', 'Description de votre blog') !!}
		{!! Form::textarea('description',null,array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit('Confirmer', array('class' => 'btn btn-info center')) !!}
	</div>
	{!! Form::close() !!}
</section>
@stop