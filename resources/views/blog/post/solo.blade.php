@extends('layout')

@section('body')
<section class="blogHeader">
	<h1><a href=".">{{ $blog->name }}</a></h1>
	<em>{{ $blog->description }}</em>
	<div id="rightHeaderBlog">
		<a class="btn btn-success subBlogBtn" href="follow/">+ S'abonner</a>
	</div>
</section>
<div class="blogPostCont">
	<div class="soloPagePost">
		<p>{!! $post->content !!}</p>
		@if ($post->media()->first())
		@if ($post->type == 0)
		@include('blog.type.text')
		@elseif ($post-> type == 1)
		@include('blog.type.photo')
		@elseif ($post->type == 2)
		@include('blog.type.quote')
		@elseif ($post->type == 3)
		@include('blog.type.music')
		@elseif ($post->type == 4)
		@include('blog.type.video')
		@endif
		@endif
	</div>
	<div class="commentsCont">

		<!-- Commentaires -->
		<div class="addComment">
		@if (Auth::check())
			{!! Form::open(array('url' => '/comment')) !!}
			{!! Form::textarea('comment',null,array('class' => 'form-control')) !!}
			@if (!Auth::check())
			{!! Form::text('author',null,array('class' => 'form-control')) !!}
			@endif
			{!! Form::hidden('post', $post->id) !!} {!! Form::submit('Envoyer', array('class' => 'btn btn-info btn-lg')) !!}
			{!! Form::close() !!}
		@else
		<b>Vous devez être connecter pour poster un commentaire</b>
		@endif
		</div>
	</div>
</div>
</div>
@stop

@section('js')
<script>
	$(document).ready(function() {
		var body = $('body');
		plyr.setup();
		body.css('background', 'linear-gradient(#eee,#fff) fixed');

		(function(d, p){
			var a = new XMLHttpRequest(),
			b = d.body;
			a.open("GET", p, true);
			a.send();
			a.onload = function(){
				var c = d.createElement("div");
				c.style.display = "none";
				c.innerHTML = a.responseText;
				b.insertBefore(c, b.childNodes[0]);
			}
		})(document, "../bower/plyr/dist/sprite.svg"); 


		function getComments() {
			var page = location.href.split('/'),
			id = page[4];

			var template;

			var thisComments = $.get('/comment/' + id).done(function (data) {
				data.forEach(function (d) {
					template = "<div class='soloComment'><em>" + d.author + "</em><i>" + d.created_at + "</i><p>" + d.comment + "</p></div>";
					$('.commentsCont').prepend(template);
				})
			});
		}

		getComments();
	}); 
</script>
@stop