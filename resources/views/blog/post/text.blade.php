<div class="modal fade newPostModal">
	<div class="modal-dialog loginModal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => '/new', 'class' => 'newPostForm', 'files' => true)) !!}
				<select name="blog">
					@foreach (Auth::user()->blog()->get() as $blog)
					<option value="{{$blog->id}}">{{ $blog->name }}</option>
					@endforeach
				</select>

				<div class="formWrapper">
				</div>
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
				<input type="submit" class="btn btn-primary" id="addNewQuestion" value="Poster">
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
