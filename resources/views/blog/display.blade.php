@extends('layout')
@section('body')
<div id="headerBann">
<div id="opac"></div>
</div>
<section class="blogHeader">
	<h1><a href="">{{ $blog->name }}</a></h1>
	<em>{{ $blog->description }}</em>
	<div id="rightHeaderBlog">
	@if (Auth::check() && Auth::user()->id != $blog->owner)
		<a class="btn btn-success subBlogBtn" href="follow/{{$blog->id }}">+ S'abonner</a>
	@endif
	<b id="contactTheAuthor">contacter l'auteur du blog</b>
	<section id="contactBlog" class="none">
		{!! Form::open(array('url' => '/newmsg')) !!}
		@if (Auth::check())
		{!! Form::hidden('author', Auth::user()->username) !!}
		{!! Form::hidden('email', Auth::user()->email) !!}
		@else
		{!! Form::text('author',null,array('class' => 'form-control', 'placeholder' => 'Votre nom')) !!}
		{!! Form::email('email',null,array('class' => 'form-control', 'placeholder' => 'Votre e-mail')) !!}
		@endif
		{!! Form::textarea('message',null,array('class' => 'form-control')) !!}
		{!! Form::hidden('receiver', $blog->user->id) !!}
		{!! Form::submit('Envoyer', array('class' => 'form-control')) !!}
		{!! Form::close() !!}
	</section>
	</div>
</section>
<div class="blogPostCont">
	@if (!empty ($blog->posts()->first()))
	@foreach ($blog->posts()->orderBy('created_at', 'desc')->get() as $post)
	<div class="blogSoloPost">
		<span class="datePost"><a href="{{$blog->url}}/{{$post->id}}">{{ $post->created_at }}</a></span class="datePost">
			@if ($post->type == 0)
			@include('blog.type.text')
			@elseif ($post->type == 1)
			@include('blog.type.photo')
			@elseif ($post->type == 2)
			@include('blog.type.quote')
			@elseif ($post->type == 3)
			@include('blog.type.music')
			@elseif ($post->type == 4)
			@include('blog.type.video')
			@endif
			<a href="{{URL::to('ajax/repost/'. $post->id.'/8') }}">Repost</a>
		</div>
		@endforeach
		@else
		<div class="blogSoloPost">
			<strong>Aucun posts sur ce blog</strong>
		</div>
		@endif
	</div>
	@stop
	@section('js')
	<script>
		$(document).ready(function() {

			var body = $('body');
			plyr.setup();
			body.css('background', 'linear-gradient(#eee,#fff) fixed');

			var bannCont = $('#headerBann'),
				bannFile,
				contactBtn = $('#contactTheAuthor'),
				contactBox = $('#contactBlog');

			$.get('/ajax/bann', {blog: 'test'}).done(function (data) {
				bannCont.append('<img src="../data/<?php echo $blog->user->username;?>'+ '/' + data + '" class="bann">');
			});
			console.log(bannFile);

			contactBtn.click(function(event) {
				$(this).fadeOut('fast', function () {
					contactBox.fadeIn('fast');
				})
			});

			(function(d,p){
				var a = new XMLHttpRequest(),
				b = d.body;
				a.open("GET", p, true);
				a.send();
				a.onload = function(){
					var c = d.createElement("div");
					c.style.display = "none";
					c.innerHTML = a.responseText;
					b.insertBefore(c, b.childNodes[0]);
				}
			})(document, "bower/plyr/dist/sprite.svg");	
		});
	</script>
	@stop