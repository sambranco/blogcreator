@extends('layout')

@section('body')
<section class="registerSection container">
	@if (!empty($errors))
	{{ $errors->first() }}
	@endif
<!-- 	<div class="aboutRegister">
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque et facilis placeat nobis asperiores exercitationem rem magni consequatur sequi deleniti id perferendis repellat quos, error beatae architecto temporibus, amet repellendus.</p>
</div> -->
	<div class="contRegisterInput">
		{!! Form::open(array('class' => 'FlowupLabels')) !!}
		<div class="form-group fl_wrap">
			{!! Form::label('username', 'Username', array('class' => 'fl_label')) !!}
			{!! Form::text('username',null,array('class' => 'form-control fl_input ')); !!}
		</div>
		<!-- <span class="underUsername">Votre username sera le nom de votre blog</span>-->
		<div class="form-group fl_wrap">
			{!! Form::label('email', 'Adresse e-mail', array('class' => 'fl_label')) !!}
			{!! Form::text('email',null,array('class' => 'form-control fl_input')) !!}
		</div>
		<div class="form-group fl_wrap">
			{!! Form::label('password', 'Mot de passe', array('class' => 'fl_label')) !!}
			{!! Form::password('password',array('class' => 'form-control fl_input ')) !!}
		</div>
		<div class="form-group fl_wrap">
			{!! Form::label('password2', 'Confirmation', array('class' => 'fl_label')) !!}
			{!! Form::password('password2',array('class' => 'form-control fl_input ')) !!}
		</div>
		<!-- <div class="fileUpload btn btn-info">
			<span>Avatar</span>
			{!! Form::file('avatar') !!}
		</div> -->
		<div class="form-group">
			{!! Form::submit('Valider',array('class' => 'btn subBtn')) !!}
		</div>
		{!! Form::close() !!}
	</div>
</section>
@stop

@section('js')
<script>
	$('.FlowupLabels').FlowupLabels({
        /*
        These are all the default values
        You may exclude any/all of these options
        if you won't be changing them
        	*/
        // Handles the possibility of having input boxes prefilled on page load
        feature_onLoadInit: true, 

        // Class when focusing an input
        class_focused:      'focused',
        // Class when an input has text entered
        class_populated:    'populated' 
    });
</script>
@stop