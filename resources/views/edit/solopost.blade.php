@extends('layout')
@section('body')
<span id="goBack" class="btn btn-default"><i class="fa fa-arrow-left"></i> retour</span>
<div class="well soloPostWell">
	@if ($post->title)
	<h2>{{ $post->title }}</h2>
	@endif
	@if ($post->content)
	<p>{!! $post->content !!}</p>
	@endif
	<ul>
		<li>Posté le: {{ $post->created_at }}</li>
		<li>Dernière modification le: {{ $post->updated_at }}</li>
	</ul>
	@if ($post->media->first())
	<a href="../../data/{{ $post->blog->user->username }}/{{ $post->media->first()->path }}" target="_top">Voir le média</a>
	@endif
</div>
<div class="well soloPostComments">
	<h2>Commentaires</h2>
	<table>
		<thead>
			<th>Auteur</th>
			<th>Commentaire</th>
			<th>Date</th>
			<th>Action</th>
		</thead>
		<tbody>
			@foreach ($comments as $comment)
			<tr>
				<td>{{ $comment->author}}</td>
				<td>{{ $comment->comment }}</td>
				<td>{{ $comment->created_at }}</td>
				<td>
					<a href="/{{$post->blog->url}}/{{$comment->post}}" class="btn btn-primary btn-sm" target="_top">Voir</a>
					<a href="/ajax/delete/2/{{$comment->id}}" class="btn btn-danger btn-sm" >Supprimer</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@stop

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		var goBack = $('#goBack');

		goBack.click(function () {
			history.go(-1);
		})

		$('.soloPostWell h2').click(function (event) {
			$(this).replaceWith('p', function() {
				console.log('ok');
			});
		});
	});
</script>
@stop