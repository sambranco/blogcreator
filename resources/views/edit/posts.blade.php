@extends('layout')
@section('body')
<table class="table table-hover table-striped">
	<thead>
		<th>ID</th>
		<th>Type</th>
		<th>Titre</th>
		<th>Contenu</th>
		<th>Commentaires</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach ($posts->posts as $post)
		<tr>
			<td>{{ $post->id }}</td>
			<td>
				@if ($post->type == 0)
				<i class="fa fa-pencil"></i>
				@elseif ($post->type == 1)
				<i class="fa fa-photo"></i>
				@elseif ($post->type == 2)
				<i class="fa fa-citation"></i>
				@elseif ($post->type == 3)
				<i class="fa fa-music"></i>
				@elseif ($post->type == 4)
				<i class="fa fa-film"></i>
				@endif
			</td>
			<td>{{ $post->title }}</td>
			<td>{{ str_limit($post->content, 50, '...') }}</td>
			<td>
				@if (count($post->comments) == 0)
				Aucun commentaires
				@elseif (count($post->comments) == 1)
				1 commentaire
				@else
				{{ count($post->comments) }} commentaires
				@endif
			</td>
			<td>
				<a href="{{ URL::to($post->blog->url. '/'.$post->id) }}" class="btn btn-warning btn-xs" target="_top">Voir</a>
				<a href="{{$post->id}}" class="btn btn-warning btn-xs">Editer</a>
				<a href="/ajax/delete/1/{{$post->id}}" class="btn btn-danger btn-xs">Supprimer</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@stop