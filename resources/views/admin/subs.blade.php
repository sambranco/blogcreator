@extends('layout')

@section('body')
<table>
	<thead>
		<th>Auteur</th>
		<th>Blog</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach ($subs as $sub)
		@foreach (BlogCreator\Blog::where('id', $sub->blog)->get() as $blog)
		<tr>
			<td>{{ $blog->user->username }}</td>
			<td>{{ $blog->name }}</td>
			<td><a href="/ajax/delete/3/{{$sub->id}}" class="btn btn-warning btn-xs">Se désabonner</a></td>
		</tr>
		@endforeach
		@endforeach
	</tbody>
</table>
@stop