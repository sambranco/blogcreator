@extends('layout')

@section('body')
<table class="table">
	<thead>
		<th>ID</th>
		<th>From :</th>
		<th>Message :</th>
		<th>Action :</th>
	</thead>
	<tbody>
	@foreach($messages as $msg)
		<tr>
		<td>{{$msg->id }}</td>
		<td>{{$msg->sender }}</td>
		<td>{{$msg->message }}</td>
		<td><a href="ajax/delete/4/{{$msg->id}}" class="btn btn-danger btn-xs">Supprimer</a></td>
		</tr>
	@endforeach
	</tbody>
</table>
@stop