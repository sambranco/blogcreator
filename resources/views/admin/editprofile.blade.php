@extends('layout')

@section('body')

<section class="container">
	<div class="well">
		{!! Form::open() !!}
		<div class="form-group">
			{!! Form::label('email') !!}
			{!! Form::text('email', $user->email, array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('username') !!}
			{!! Form::text('username', $user->username, array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Envoyer', array('class' => 'btn btn-info center ')) !!}
		</div>
		{!! Form::close() !!}
	</div>
	<a href="/ajax/delete/5/{{$user->id}}" class="btn btn-danger btn-lg btn-block">Supprimer mon compte</a>
</section>
@stop