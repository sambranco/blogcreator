<!DOCTYPE html>
<html>
<head>
	<title>BlogCreator</title>
	{!! HTML::style('css/bswatch.css') !!}
	{!! HTML::style('bower/font-awesome/css/font-awesome.min.css') !!}
	{!! HTML::style('css/flowlabel.css') !!}
	{!! HTML::style('bower/plyr/dist/plyr.css') !!}
	{!! HTML::style('css/main.css') !!}
	{!! HTML::script('js/id3.js') !!}
	<body>
		@yield('body')

		{!! HTML::script('bower/jquery/dist/jquery.min.js') !!}
		{!! HTML::script('bower/bootstrap/dist/js/bootstrap.min.js') !!}
		{!! HTML::script('js/flowlabel.js') !!}
		{!! HTML::script('bower/plyr/dist/plyr.js') !!}
		{!! HTML::script('main.js') !!}
		@yield('js')
	</body>
	</html>