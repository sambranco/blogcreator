@extends('layout')

@section('body')
<section id="contRoot">
	<header>
		<h1>ROOT</h1>
	</header>
	<section class="container rootCont">
		<div class="well rootWell" id="rootUser">
			<iframe src="root/users" seamless="seamless"></iframe>
		</div>
		<div class="well rootWell">
			<iframe src="root/blogs" seamless="seamless"></iframe>
		</div>
	</section>
</section>
@stop

@section('js')
<script>

</script>
@stop