@extends('layout')

@section('body')
<h2>Blogs</h2>
<table class="table table-hover table-striped">
	<thead>
		<th>ID</th>
		<th>Name</th>
		<th>Description</th>
		<th>Active</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach ($blogs as $blog)
		<tr>
			<td>{{ $blog->id }}</td>
			<td>{{ $blog->name }}</td>
			<td>{{ $blog->description }}</td>
			<td>{{ $blog->active }}</td>
			<td>
				<button class="btn btn-warning btn-xs">un</button>
				<button class="btn btn-warning btn-xs">deux</button>
				<button class="btn btn-warning btn-xs">trois</button>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{!! $blogs->render() !!}
@stop