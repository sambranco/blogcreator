@extends('layout')

@section('body')
<h2>Users</h2>
<table class="table table-hover table-striped">
	<thead>
		<th>ID</th>
		<th>Username</th>
		<th>Email</th>
		<th>Active</th>
		<th>Member since</th>
		<th>Action</th>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->id }}</td>
			<td>{{ $user->username }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->active }}</td>
			<td>{{ $user->created_at }}</td>
			<td>
				<button class="btn btn-warning btn-xs">un</button>
				<button class="btn btn-warning btn-xs">deux</button>
				<button class="btn btn-warning btn-xs">trois</button>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{!! $users->render() !!}
@stop